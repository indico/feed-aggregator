FROM python:3.12-slim

# create an unprivileged user to run as
RUN set -ex && \
	groupadd -r appuser && \
	useradd -r -g appuser -m -d /app appuser

WORKDIR /app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY app.py defaults.cfg ./

USER appuser
CMD ["gunicorn", "app:app", "--bind", "0.0.0.0:8080", "--threads", "10", "--capture-output"]
EXPOSE 8080
