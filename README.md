# feed-aggregator

This is a very simple app that simply aggregates Indico feeds from multiple categories and exposes
them as a single feed, limited to basic data.

It does not use any kind of authentication on purpose because it's meant just for public events.
