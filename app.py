import os
from datetime import datetime

import requests
import yaml
from bleach import Cleaner, html5lib_shim
from bleach.css_sanitizer import CSSSanitizer
from flask import Flask, jsonify, request
from flask_caching import Cache
from lxml import etree
from pytz import timezone, utc
from requests import RequestException


BLEACH_ALLOWED_TAGS = {
    'a', 'abbr', 'acronym', 'b', 'blockquote', 'code', 'em', 'i', 'li', 'ol', 'strong', 'ul', 'sup', 'sub', 'small',
    'br', 'p', 'table', 'thead', 'tbody', 'th', 'tr', 'td', 'img', 'hr', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'pre',
    'dl', 'dd', 'dt', 'figure', 'blockquote', 'center', 'div', 'span', 'strike', 'u',
}
BLEACH_ALLOWED_ATTRIBUTES = {
    'a': ['href', 'title'],
    'abbr': ['title'],
    'acronym': ['title'],
    'img': ['src', 'alt'],
    '*': ['align'],
}
BLEACH_ALLOWED_STYLES = []


app = Flask(__name__)
app.json.sort_keys = False

app.config.from_pyfile('defaults.cfg')
if configfile := os.environ.get('IFAGG_CONFIG'):
    app.config.from_file(configfile, yaml.safe_load)

cache = Cache(app)


def _deserialize_date(date_dict):
    dt = datetime.fromisoformat(f'{date_dict['date']}T{date_dict['time']}')
    return timezone(date_dict['tz']).localize(dt).astimezone(utc)


class AbsoluteURLsFilter(html5lib_shim.Filter):
    """html5lib filter that makes URLs absolute."""

    def __init__(self, source, base_url):
        super().__init__(source)
        self.base_url = base_url

    def _make_absolute(self, attrs, attr):
        key = (None, attr)
        try:
            value = attrs[key]
        except KeyError:
            return
        if value.startswith('/') and not value.startswith('//'):
            attrs[key] = self.base_url.rstrip('/') + value

    def __iter__(self):
        for token in super().__iter__():
            if token['type'] in ['StartTag', 'EmptyTag']:
                if token['name'] == 'img':
                    self._make_absolute(token['data'], 'src')
                elif token['name'] == 'a':
                    self._make_absolute(token['data'], 'href')
            yield token


def _cleanup_html(string, base_url):
    cleaner = Cleaner(
        tags=BLEACH_ALLOWED_TAGS,
        attributes=BLEACH_ALLOWED_ATTRIBUTES,
        css_sanitizer=CSSSanitizer(allowed_css_properties=BLEACH_ALLOWED_STYLES),
        filters=[lambda source: AbsoluteURLsFilter(source, base_url)]
    )
    return cleaner.clean(string)


def _html_to_plaintext(string):
    string = string.replace('\n', '').strip()
    if not string:
        return ''
    doc = etree.HTML(string)
    for elem in doc.xpath('//p | //br | //h1 | //h2 | //h3 | //h4 | //h5 | //h6'):
        elem.tail = '\n' + elem.tail if elem.tail else '\n'
    return doc.xpath('string()').strip()


def _make_logo_map(chains, logos):
    mapping = {}
    for chain in chains:
        for segment in chain['path']:
            if segment.get('type') != 'Category':  # last segment is visibility information
                continue
            if logo := logos.get(segment['id']):
                mapping[chain['categoryId']] = logo
    return mapping


def _cleanup_api_data(data, logos, base_url):
    entries = data.pop('results')
    logo_map = _make_logo_map(data['additionalInfo']['eventCategories'], logos)
    converted = []
    for entry in entries:
        converted.append({
            'id': int(entry['id']),
            'category_id': entry['categoryId'],
            'category': entry['category'],
            'logo_url': logo_map.get(entry['categoryId'], logos.get('default')),
            'start_dt': _deserialize_date(entry['startDate']).isoformat(),
            'end_dt': _deserialize_date(entry['endDate']).isoformat(),
            'venue': entry['location'].strip(),
            'room': entry['room'].strip(),
            'url': entry['url'],
            'title': entry['title'],
            'description_html': _cleanup_html(entry['description'], base_url),
            'description_text': _html_to_plaintext(entry['description']),
        })
    return converted


@app.route('/')
def index():
    return 'nothing here'


@app.route('/feed/<name>')
@cache.cached(timeout=300, query_string=True)
def feed(name):
    from_date = request.args.get('from') or 'today'
    to_date = request.args.get('to')

    try:
        feed = app.config['FEEDS'][name]
    except KeyError:
        resp = jsonify(error='invalid-feed')
        resp.status_code = 404
        return resp

    base_url = feed['indico_url']
    cat_string = '-'.join(map(str, feed['category_ids']))
    url = f'{base_url}/export/categ/{cat_string}.json'

    params = {
        'tz': 'Europe/Zurich',
        'onlypublic': 'yes',
        'from': from_date
    }
    if to_date:
        params['to'] = to_date

    try:
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        data = resp.json()
    except RequestException:
        resp = jsonify(error='request-failed')
        resp.status_code = 503
        return resp

    return _cleanup_api_data(data, feed['logos'], base_url)
